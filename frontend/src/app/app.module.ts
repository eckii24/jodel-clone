import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PostsComponent} from './posts/posts.component';
import {PostsEditComponent} from './posts/posts-edit/posts-edit.component';
import {CommentsComponent} from './comments/comments.component';
import {CommentsEditComponent} from './comments/comments-edit/comments-edit.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {PostComponent} from './posts/post/post.component';
import {HeaderComponent} from './header/header.component';
import {FormsModule} from "@angular/forms";
import {CommentComponent} from './comments/comment/comment.component';
import { ContextMenuModule } from 'ngx-contextmenu';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PostsEditComponent,
    CommentsComponent,
    CommentsEditComponent,
    LoginComponent,
    RegisterComponent,
    PostComponent,
    HeaderComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ContextMenuModule.forRoot({useBootstrap4: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
