import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PostsComponent} from "./posts/posts.component";
import {CommentsComponent} from "./comments/comments.component";
import {PostsEditComponent} from "./posts/posts-edit/posts-edit.component";
import {CommentsEditComponent} from "./comments/comments-edit/comments-edit.component";
import {LoginComponent} from "./auth/login/login.component";

const routes: Routes = [
  {path: "", redirectTo: "posts", pathMatch: "full"},
  {path: "login", component: LoginComponent},
  {path: "posts", component: PostsComponent, pathMatch: "full"},
  {path: "posts/new", component: PostsEditComponent, pathMatch: "full"},
  {path: "posts/:id/edit", component: PostsEditComponent, pathMatch: "full"},
  {path: "posts/:id/comments", component: CommentsComponent, pathMatch: "full"},
  {path: "posts/:id/comments/new", component: CommentsEditComponent, pathMatch: "full"},
  {path: "posts/:id/comments/:id_comment/edit", component: CommentsEditComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
