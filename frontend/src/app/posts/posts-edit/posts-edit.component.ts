import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DataService} from "../data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-posts-edit',
  templateUrl: './posts-edit.component.html',
  styleUrls: ['./posts-edit.component.css']
})
export class PostsEditComponent implements OnInit {

  @Output() postCreated = new EventEmitter<boolean>();

  constructor(private dataService: DataService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form) {
    this.dataService.postPost(form.value.body).subscribe(
      () => {
        this.postCreated.emit(true);
        form.reset()
      }
    )
  }

}
