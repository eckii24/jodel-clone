import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Post} from "./post.model";
import {DataService} from "../data.service";
import {AuthService} from "../../auth/auth.service";
import {ContextMenuComponent} from 'ngx-contextmenu';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() post: Post;
  @Output() postChanged = new EventEmitter<boolean>();
  @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
  showComments: boolean = false;
  editMode: boolean = false;
  lastBody: string;

  constructor(private dataService: DataService,
              public authService: AuthService) {
  }

  ngOnInit() {
  }

  onClickUp() {
    this.post.rating += 1;
    this.dataService.postRating(this.post.id, "+").subscribe(
      () => {
        this.postChanged.emit(true)
      }
    )
  }

  onClickDown() {
    this.post.rating -= 1;
    this.dataService.postRating(this.post.id, "-").subscribe(
      () => {
        this.postChanged.emit(true)
      }
    )
  }

  editPost(event) {
    this.editMode = true;
    this.lastBody = this.post.body
  }

  onSubmit(form) {
    this.editMode = false;
    this.lastBody = null;
    console.log(form);
    this.dataService.putPostBody(this.post.id, form.value.body).subscribe(
      () => {
        this.postChanged.emit(true)
      }
    )
  }

  deletePost(event) {
    this.dataService.deletePost(event.item.id).subscribe(
      () => {
        this.postChanged.emit(true)
      }
    );
  }

  onAbort() {
    this.editMode = false;
    this.post.body = this.lastBody;
    this.lastBody = null;
  }
}
