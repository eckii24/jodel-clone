import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Post} from "./post/post.model";
import {map} from "rxjs/operators";
import {Comment} from "../comments/comment/comment.model";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private URL: string = "http://localhost:5001";

  constructor(private http: HttpClient) {
  }

  getPosts() {
    return this.http.get<Post[]>(this.URL + "/posts").pipe(map(
      (posts: Post[]) => {
        posts.sort((first,second) => {return second.rating - first.rating});
        return posts
      }
    ))
  }

  postPost(body: string) {
    return this.http.post(this.URL + "/posts", {body: body})
  }

  getComments(id: number) {
    return this.http.get<Comment[]>(this.URL + `/posts/${id}/comments`)
  }

  postRating(id: number, char: string) {
    return this.http.put(this.URL + `/posts/${id}/${char}`, null)
  }


  doLogin(name: string, password: string) {
    return this.http.post(this.URL + `/login/${name}/${password}`, null)
  }

  postComment(id: number, body: any) {
    return this.http.post(this.URL + `/posts/${id}/comments`, {body: body})
  }

  deletePost(id: number) {
    return this.http.delete(this.URL + `/posts/${id}`)
  }

  putPostBody(id:number, body: string) {
    return this.http.put(this.URL + `/posts/${id}`, {body: body})
  }
}
