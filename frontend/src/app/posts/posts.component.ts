import {Component, OnInit} from '@angular/core';
import {DataService} from "./data.service";
import {Post} from "./post/post.model";
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];

  constructor(private dataService: DataService,
              public authService: AuthService) {
  }

  ngOnInit() {
    this.loadPosts()
  }

  loadPosts() {
    this.dataService.getPosts().subscribe((posts: Post[]) => {
      this.posts = posts;
    })
  }
}
