import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "../../posts/data.service";
import {Post} from "../../posts/post/post.model";

@Component({
  selector: 'app-comments-edit',
  templateUrl: './comments-edit.component.html',
  styleUrls: ['./comments-edit.component.css']
})
export class CommentsEditComponent implements OnInit {

  @Input() post:Post;
  @Output() commentCreated = new EventEmitter<boolean>();

  constructor(private dataService:DataService) { }

  ngOnInit() {
  }

  onSubmit(form){
    this.dataService.postComment(this.post.id, form.value.body).subscribe(
      ()=>{
        this.commentCreated.emit(true);
        form.reset()
      }
    )
  }

}
