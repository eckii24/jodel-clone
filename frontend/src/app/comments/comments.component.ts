import {Component, Input, OnInit} from '@angular/core';
import {Post} from "../posts/post/post.model";
import {DataService} from "../posts/data.service";
import {AuthService} from "../auth/auth.service";
import {Comment} from "./comment/comment.model";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  @Input() post: Post;
  comments: Comment[];

  constructor(private dataService: DataService,
              public authService: AuthService) {
  }

  ngOnInit() {
    this.dataService.getComments(this.post.id)
      .subscribe(
        (comments:Comment[]) => {
          this.comments = comments;
        }
      )
  }

  commentCreated(){
    this.dataService.getComments(this.post.id)
      .subscribe(
        (comments:Comment[]) => {
          this.comments = comments;
        }
      )
  }

}
