import {Component, OnInit} from '@angular/core';
import {DataService} from "../../posts/data.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private dataService: DataService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
  }

  onSubmit(form){
    this.checkLogin(form.value.name, form.value.password)
  }

  checkLogin(name: string, password: string) {
    this.dataService.doLogin(name, password).subscribe((response) => {
        this.authService.isAuthenticated = true;
        this.authService.token = response["token"];
        this.router.navigate(["/"])
      },
      (error) => {
        this.authService.isAuthenticated = false;
        this.authService.token = null;
        alert("Eingabe fehlerhaft!")
      });
  }

}
