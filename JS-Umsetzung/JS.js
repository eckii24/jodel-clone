// server.js
// where your node app starts

// init project
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function (req, res) {
  res.send("hello world");
});

app.get('/user/:id', function (req, res) {
  let obj = {name: req.params.id, date: Date.now()};
  res
    .type("application/json")
    .status(200)
    .send(obj);
});

app.put('/user/:id', upload.array(), function (req, res) {
  let obj = {name: req.params.id, date: Date.now(), success: true, data: req.body};
  res
    .type("application/json")
    .status(200)
    .send(obj);
});

app.post('/user/:id', upload.array(), function (req, res) {
    let obj = {name: req.params.id, date: Date.now(), success: true, data: req.body};
    res
      .type("application/json")
      .status(200)
      .send(obj);
  });

app.delete('/user/:id', upload.array(), (req, res)=>{
    res.send(req.body)
})

// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log('Your app is listening on port ' + listener.address().port);
});
