// IMPORTS SERVER
const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const cookieParser = require('cookie-parser');
const uuid = require("uuid");

// KONSTANTEN SERVER
const app = express();
const upload = multer();
const SESSIONS = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(function (request, response, next) {
    //request.isAuthenticated = SESSIONS.indexOf(request.cookies.token) !== -1;
    request.isAuthenticated = true;
    response.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    response.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// IMPORT DB
const fs = require("fs");
const sqlite3 = require("sqlite3").verbose();

// KONSTANTEN und VARIABLEN DB
const dbFile = "./JS-Umsetzung/.data/sqlite.db";
let exists = fs.existsSync(dbFile);
const db = new sqlite3.Database(dbFile);

// DB-Initialisierung
db.serialize(() => {
    if (!exists) {
        db.run(`CREATE TABLE Post (
          id     INTEGER PRIMARY KEY AUTOINCREMENT,
          body   VARCHAR(255),
          rating INTEGER(2)
        )`);
        db.run(`CREATE TABLE Comment (
          id      INTEGER PRIMARY KEY AUTOINCREMENT,
          id_post INTEGER(2),
          body    VARCHAR(255)
        )`);
        db.run(`CREATE TABLE User (
          id       INTEGER PRIMARY KEY AUTOINCREMENT,
          name     VARCHAR(255),
          password VARCHAR(255)
        )`);
        console.log("DB erstellt");
    }
});


// HTTP-GET
app.get("/posts", (request, response) => {
    db.all("SELECT * FROM Post", (error, rows) => {
        response.send(rows);
    });
});

app.get("/posts/:id", (request, response) => {
    db.get(`SELECT *
            FROM Post
            WHERE id = ?`,
        request.params.id,
        (error, row) => {
            if (row) {
                response.send(row);
            } else {
                response.status(404).send({error: "Record not found"});
            }
        }
    );
});

app.get("/posts/:id/comments", (request, response) => {
    db.all(`SELECT *
            FROM Comment
            WHERE id_post = ?`,
        request.params.id,
        (error, rows) => {
            if (rows) {
                response.send(rows);
            } else {
                response.status(404).send({error: "Record not found"});
            }
        }
    );
});

app.get("/posts/:id/comments/:id_comment", (request, response) => {
    db.get(
            `SELECT *
             FROM Comment
             WHERE id_post = ?
               AND id = ?`,
        request.params.id,
        request.params.id_comment,
        (error, row) => {
            if (row) {
                response.send(row);
            } else {
                response.status(404).send({error: "Record not found"});
            }
        }
    );
});


// HTTP-POST
app.post("/posts", upload.array(), (request, response) => {
    if (request.isAuthenticated) {
        db.run(`INSERT INTO Post (body, rating)
                VALUES (?, 0)`,
            request.body.body,
            function () {
                const id = this.lastID;
                db.get('SELECT * FROM Post WHERE id = ?', id, (error, row) => {
                    console.log(id, row, this);
                    response.status(201).send(row);
                });
            }
        );
    } else {
        response.status(401).send({error: "Not authenticated"})
    }

});

app.post("/posts/:id/comments", upload.array(), (request, response) => {
    if (request.isAuthenticated) {
        db.run(`INSERT INTO Comment (id_post, body)
                VALUES (?, ?)`,
            request.params.id,
            request.body.body,
            function () {
                const id = this.lastID;
                db.get("SELECT * FROM Comment WHERE id = ?", id, (error, row) => {
                    response.status(201).send(row);
                });
            }
        );
    } else {
        response.status(401).send({error: "Not authenticated"})
    }
});


// HTTP-PUT und HTTP-DELETE
app.put("/posts/:id/", upload.array(), (request, response) => {
    if (request.isAuthenticated) {
        db.get(`SELECT *
                FROM Post
                WHERE id = ?`,
            request.params.id,
            (error, rows) => {
                if (rows) {
                    console.log(request.body);
                    db.run(`UPDATE Post
                            SET body = ?
                            WHERE id = ?`,
                        request.body.body,
                        request.params.id
                    );
                    db.get(`SELECT *
                            FROM Post
                            WHERE id = ?`,
                        request.params.id,
                        (error, row) => {
                            response.send(row);
                        }
                    );

                } else {
                    response.status(404).send({error: "Record not found"});
                }
            }
        );
    } else {
        response.status(401).send({error: "Not authenticated"})
    }
});

app.put("/posts/:id/:rating", upload.array(), (request, response) => {
    if (request.isAuthenticated) {
        db.get(`SELECT *
                FROM Post
                WHERE id = ?`,
            request.params.id,
            (error, row) => {
                if (row) {
                    let newRating = row.rating + (request.params.rating === "+" ? 1 : -1);
                    db.run(`UPDATE Post
                            SET rating = ?
                            WHERE id = ?`,
                        newRating,
                        request.params.id
                    );
                    db.get(`SELECT *
                            FROM Post
                            WHERE id = ?`,
                        request.params.id,
                        (error, row) => {
                            response.send(row);
                        }
                    );
                } else {
                    response.status(404).send({error: "Record not found"});
                }
            }
        );
    } else {
        response.status(401).send({error: "Not authenticated"})
    }
});

app.delete("/posts/:id", (request, response) => {
    if (request.isAuthenticated) {
        db.get(`SELECT *
                FROM Post
                WHERE id = ?`,
            request.params.id,
            (error, rows) => {
                if (rows) {
                    db.run(`DELETE
                            FROM Post
                            WHERE id = ?`, request.params.id);
                    response.status(204).send();
                } else {
                    response.status(404).send({error: "Record not found"});
                }
            }
        );
    } else {
        response.status(401).send({error: "Not authenticated"})
    }
});


// User-Verwaltung
app.post("/login/:name/:password", (request, response) => {
    db.get(`SElECT *
            FROM User
            WHERE name = ?
              AND password = ?`, request.params.name, request.params.password, (error, row) => {
        if (row) {
            let token = uuid();
            SESSIONS.push(token);
            response.cookie('token', token).send({token: token});
        } else {
            response.status(401).send({error: "Not authenticated"})
        }
    })
});

app.post("/register/:name/:password", (request, response) => {
    db.run(`INSERT INTO User (name, password)
            VALUES (?, ?)`,
        request.params.name,
        request.params.password,
        () => {
            db.all(`SELECT name
                    FROM User`, (error, rows) => {
                response.status(201).send(rows);
            });
        }
    );
});


// Methode um Quick & Dirty die DB anzupassen
app.post("/setup", (request, response) => {
    db.run(`INSERT INTO User (name, password)
            VALUES ('Matthias', 'PW')`)
});


// Starten
const listener = app.listen(5001, () => {
    console.log("Your app is listening on port " + listener.address().port);
});
