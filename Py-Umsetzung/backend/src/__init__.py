import importlib
import os
from datetime import timedelta

from flask import Flask, render_template
from flask_sslify import SSLify
from flask_restful import Api

app = Flask(__name__)
app.secret_key = "4h§$/&&ah8&§26ga155AG6554G%36664%"
app.permanent_session_lifetime = timedelta(hours=5)
# sslify = SSLify(app) # Aktuell auskommentiert, da nur auf dem Server funktionsfähig

api = Api(app)


@app.route('/')
def index():
    """
    Basis-Route zum Aufruf der von Angular erstellten Single-Page-Anwendung.

    :return: Gerenderte index.html
    """
    return render_template("index.html")


@app.errorhandler(404)
def error(e):
    return "error"


# Dynamisches Hinzufügen der einzelnen Routen für die REST-API
for module in os.listdir(__file__[:__file__.rfind("__init__.py")] + "restful"):
    if not module.endswith(".py"):
        continue

    # Laden des Moduls (ACHTUNG: HARTE KODIERUNG)
    import_module = importlib.import_module("backend.src.restful.{}".format(module[:-3]))
    if "CLASSES" not in dir(import_module):
        continue

    # Jedes Modul benötigt das Attribut "CLASSES". Dies entspricht einer Liste, in der alle relevanten Klassen
    # den Moduls enthalten sind.
    for class_name in import_module.CLASSES:
        class_obj = getattr(import_module, class_name)
        # Jede Klasse benötigt das Attribut "ROUTE". Dies entspricht einem String mit der URL-Route für diese Klasse.
        if "ROUTE" in dir(class_obj):
            api.add_resource(class_obj, class_obj.ROUTE)
