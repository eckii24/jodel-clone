from flask_restful import reqparse, abort, Resource
from flask import request, jsonify

CLASSES = ["Post", "Posts", "Comment", "Comments"]

POSTS = [
    {"todo": "todos"},
    {"todo2": "todos"}
]


def abort_if_todo_doesnt_exist(id):
    if len(POSTS) < id:
        abort(404, message="Todo {} doesn't exist".format(id))


parser = reqparse.RequestParser()
parser.add_argument('post')

class Posts(Resource):
    ROUTE = "/api/posts"

    def get(self):
        return POSTS

    def post(self):
        POSTS.append(request.json)
        return "Success", 201

class Post(Resource):
    ROUTE = "/api/posts/<int:id>"

    def get(self, id):
        abort_if_todo_doesnt_exist(id)
        return jsonify(POSTS[id])
    
    def put(self, id):
        abort_if_todo_doesnt_exist(id)
        POSTS[id] = request.json
        return "Edited", 200

class Comment(Resource):
    ROUTE = "/api/posts/<int:id_post>/comments/<int:id_comment>"

    def get(self, id):
        abort_if_todo_doesnt_exist(id)
        return jsonify(POSTS[id])
    
    def put(self, id):
        abort_if_todo_doesnt_exist(id)
        POSTS[id] = request.json
        return "Edited", 200

class Comments(Resource):
    ROUTE = "/api/posts/<int:id_post>/comments/"

    def get(self, id):
        abort_if_todo_doesnt_exist(id)
        return jsonify(POSTS[id]["comments"])
    
    def post(self, id):
        abort_if_todo_doesnt_exist(id)
        POSTS[id] = request.json
        return "Edited", 200